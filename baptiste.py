#!/usr/local/bin/ipython

import csv
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna
import sys
from collections import Counter

#questions:
#start/stop codons - do the cross product
#register - conservative choice

def ind(hyb,s):
    try:
        i = hyb.index(s)
    except:
        i = -1
    return i

def cond(hyb,start,stop):
    s = hyb[:180]
    i = ind(s,stop)
    if i>=0:
        sub_s = s[i:]
        j = ind(sub_s,start)
        if j<0:
            return False
        elif j<30:
            return False
        elif j>=30:
            return True
    return False

def cond_reg(hyb,start,stop):
	s = hyb[:180]
	i = ind(s,stop)
	if i>=0 & i%3 == 0:
		sub_s = s[i:]
		j = ind(sub_s,start)
		if j<0:
			return False
		if j<30 and j%3 == 0:
			return False
		elif j>=30 and j%3 == 0:
			return True
	return False

def tests():
	test_start_codon = 'ATG'
	test_stop_codon = 'CAT'
	a_string = "A"*180

	cond1_test_seq = Seq(a_string[:150] + test_stop_codon + a_string[153:],generic_dna)
	cond2_test_seq = Seq(a_string[:162] + test_stop_codon + a_string[165:168] + test_start_codon + a_string[171:],generic_dna)
	cond3_test_seq = Seq(a_string[:120] + test_stop_codon + a_string[123:153] + test_start_codon + a_string[156:],generic_dna)
	
	print("Length: %i of cond1 test seq, non-reg cond: %s reg cond: %s "%(len(cond1_test_seq),cond(cond1_test_seq,test_start_codon,test_stop_codon),cond_reg(cond1_test_seq,test_start_codon,test_stop_codon)))
	print("Length: %i of cond2 test seq, non-reg cond: %s reg cond: %s "%(len(cond2_test_seq),cond(cond2_test_seq,test_start_codon,test_stop_codon),cond_reg(cond2_test_seq,test_start_codon,test_stop_codon)))
	print("Length: %i of cond3 test seq, non-reg cond: %s reg cond: %s "%(len(cond3_test_seq),cond(cond3_test_seq,test_start_codon,test_stop_codon),cond_reg(cond3_test_seq,test_start_codon,test_stop_codon)))
	
def list_n_best(start_codons,stop_codons,n):
	total_start = sum([start_codons[e] for e in set(start_codons.elements())])
	total_stop = sum([stop_codons[e] for e in set(stop_codons.elements())])
	print("Start codons:")
	for i,(seq,count) in enumerate(start_codons.most_common(n)):
		print("Nr.%i, %s, %f"%(i,str(seq),100*count/total_start))
	print("")
	print("Stop codons:")
	for i,(seq,count) in enumerate(stop_codons.most_common(n)):
		print("Nr.%i, %s, %f"%(i,str(seq),100*count/total_stop))

def assess_conditions(hybrids,start,stop):
	conds = [cond(hybrid,str(start),str(stop)) for hybrid in hybrids]
	cond_regs = [cond_reg(hybrid,str(start),str(stop)) for hybrid in hybrids]
	cond_counter = Counter(conds)
	cond_regs_counter = Counter(cond_regs)
	print("No register:")
	for i,(seq,count) in enumerate(cond_counter.most_common(2)):
		print("Condition %s, %f"%(str(seq),100*count/len(hybrids)))
	print("")
	print("With register:")
	for i,(seq,count) in enumerate(cond_regs_counter.most_common(2)):
		print("Condition %s, %f"%(str(seq),100*count/len(hybrids)))

if __name__ == "__main__":
	reader = csv.reader(open("./Pseudomonas_aeruginosa_UCBPP-PA14_109.csv","r"))
	web_gene = list(open("./Pseudomonas_aeruginosa_UCBPP-PA14_109.fna","r"))[1:]
	web_gene = "".join([e.strip() for e in web_gene])
	data = list(reader)
	web_ba_seqs = list()
	hybrids = list()
	for e in data[3:]:
		if len(e)>=5:
			_from = int(e[3]) - 1
			_to = int(e[4])
			_name = e[1][:-1]
			_ba = e[-2]
			_aa = e[-1]
			seq = Seq(_ba,generic_dna)
			web_ba_seqs.append(seq)
			hybrid = Seq(web_gene[_from-180:_to],generic_dna)
			hybrids.append(hybrid)
	start_codons = Counter([seq[:3] for seq in web_ba_seqs])
	stop_codons = Counter([seq[-3:] for seq in web_ba_seqs])
		
